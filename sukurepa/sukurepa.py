import lncrawl


def search_novel(str):
    lncrawl.start_app()


def fib(n: int):
    if n < 2:
        return n
    else:
        return fib(n - 1) + fib(n - 2)
