<p align="center">
  <a href="" rel="noopener">
 <img width=256px height=256px src="https://awau.moe/5iyKMm1.png" alt="Project logo"></a>
</p>

<h3 align="center">Sukurepa</h3>

<div align="center">

[![Build Status](https://drone.miraris.moe/api/badges/teppelin/sukurepa/status.svg)](https://drone.miraris.moe/teppelin/sukurepa)

</div>

---

<p align="center"> Teppelin releases scraper.
    <br> 
</p>

## 📝 Table of Contents

-   [About](#about)
-   [Getting Started](#getting-started)
-   [Deployment](#deployment)
-   [Usage](#usage)
-   [Built Using](#built-using)
-   [TODO](../TODO.md)
-   [Contributing](../CONTRIBUTING.md)

## 🧐 About

LN/WN teppelin (releases) scraper, sends output into an AMQP queue

## 🏁 Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

-   Python 3

### Installing

```sh
# Install dependencies
pipenv install --dev

# Setup pre-commit and pre-push hooks
poetry run pre-commit install -t pre-commit
poetry run pre-commit install -t pre-push
```

End with an example of getting some data out of the system or using it for a little demo.

## 🔧 Running the tests

Explain how to run the automated tests for this system.

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## 🎈 Usage

Add notes about how to use the system.

## 🚀 Deployment

Add additional notes about how to deploy this on a live system.

## ⛏️ Built Using

-   [Python](https://www.python.org/) - env
-   [Pika](https://github.com/pika/pika) - AMQP lib
-   [lightnovel-crawler](https://code.miraris.moe/kuwoyuki/lightnovel-crawler) - root lib
